UIAutomatorViewer 실행시에 필요한 파일들
adb.exe
AdbWinApi.dll
AdbWinUsbApi.dll
uiautomatorviewer.bat

현재 실행중인 activity 알아내기
adb shell dumpsys window windows| grep -E 'mCurrentFocus|mFocusedApp'  

apk 정보 추출  
aapt dump badging xx.apk경로  

launchable activity 추출    
/aapt dump badging xx.apk | grep "launchable-activity"  

빌드 속성 리스트  
./adb shell cat system/build.prop  

전체 리스트  
./adb shell getprop  

모델명
./adb shell getprop ro.product.model  
ro.product.model: SM-G930S  

안드로이드 버전
./adb shell getprop ro.build.version.release  
ro.build.version.release: 7.0  


