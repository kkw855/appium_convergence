Nodejs 무설치판 바이너리를 다운받는다.  
압축을 푼 폴더에서 커맨드 창을 연다.   
npm install appium@1.6.4 명령으로 appium을 설치한다.  
appiun -v 명령으로 설치된 버전을 확인한다.  
appium -a 127.0.0.1 -p 4723 명령으로 appium을 실행한다.  

내가 fork한 appium/java-client 저장소  
https://github.com/kkw855/java-client  

Random Alert Dialog 제거하기  
AppiumDriverLocalService 객체를 두 개 생성한다.(2세션 생성)  
BOOTSTRAP_PORT_NUMBER, usingAnyFreePort 번호를 다르게 설정한다.  
SESSION_OVERRIDE를 true로 설정한다. 

각각의 세션별로 Driver 객체를 생성한다.  
하나는 UI 테스트 워크플로우를 수행하고, 하나는 스레드 안에서 Alert를 제거하는데 사용한다.  


https://ko.wikipedia.org/wiki/XPath  
XPath(XML Path Language) 간단한 설명  
'/'는 바로 하위를, '//'은 모든 하위를 의미한다.  

MobileElement placeNameView = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).setAsHorizontalList().scrollIntoView("  
                + "new UiSelector().text(\"" + placeName + "\"))");  
UIAutomator 객체를 사용해서 엘리먼트를 찾는다.   

driver.findElementByAccessibilityId("");

안드로이드에서 스크롤 가능한 레이아웃은 아답터를 통해서 컨텐트 엘리먼트를 동적으로 생성한다.   
따라서 findelement같은 함수로 스크롤 가능한 레이아웃이 가진 모든 엘리먼트를 한번에 찾을 수 없다.   

UiScrollable 객체는 스크롤 가능한 Layout 객체 내부의 엘리먼트를 찾는걸 돕는다.
UiScrollable setAsHorizontalList()  
가로로 스크롤하게 설정한다.  
UiScrollable setAsVerticalList()  
세로로 스크롤하게 설정한다.  
scrollIntoView(UiSelector selector);    
엘리먼트가 보일 때까지 계속 스크롤 한다.  

MobileBy.AndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView("  
                                + "new UiSelector().text(\"Radio Group\"));"));  
Radio Group 아이템이 화면에 보일 때까지 계속 스크롤한다.   
            

MobileElement popUpElement = driver.findElement(MobileBy.AccessibilityId("Make a Popup!")); 
content-desc속성으로 엘리먼트를 찾는다.(대소문자 구분)   
driver.findElement(By.xpath(".//*[@text='Search']"));  
text속성으로 엘리먼트를 찾는다.   


final WebDriverWait wait = new WebDriverWait(driver, 10);    
명시한 조건이 충족될 때까지 기다린다. 지정한 시간까지 예외는 무시한다.   
    .ignore();  

wait.until(ExpectedConditions.presenceOfElementLocated(By));  
DOM of page에 엘리먼트가 존재하는지 확인한다. 엘리먼트가 반드시 visible이라는 의미는 아니다.   

wait.until(ExpectedConditions.visibilityOf(element));  
엘리먼트가 DOM of a page에 존재하고 visible이다. Visibility는 엘리먼트가 화면에 보이고 또한  
화면에서 높이와 폭이 0보다 크다는 것을 의미한다.  

wait.until(ExpectedConditions.presenceOfElementLocated(  
            By.xpath("//*[@text='Clicked popup menu item Search']")  
안드로이드 Toast를 찾을 수 있다.(맨앞에 '.'이 없는 것에 주의한다.)  

driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
최소 한개의 엘리먼트를 찾을 때까지 예외를 던지지 않고 기다리도록 설정한다.  
테스트 실행 시간에 부정적인 영향을 끼칠 수도 있다.  



DeviceRotation landscapeRightRotation = new DeviceRotation(0, 0, 90);  
오른쪽 방향으로 회전한다.  
DeviceRotation landscapeLeftRotation = new DeviceRotation(0, 0, 270);  
왼쪽 방향으로 회전한다.  
DeviceRotation landscapeLeftRotation = new DeviceRotation(0, 0, 270);  
화면을 가로방향으로 설정한다.  
driver.rotate(landscapeRightRotation);  
assertEquals(driver.rotation(), landscapeRightRotation);  
현재 폰 화면에 설정된 DeviceRotation 객체를 리턴한다.  


driver.pressKeyCode(AndroidKeyCode.BACK);  
키보드를 입력한다. 
boolean b = driver.isKeyboardShown();  
현재 소프트웨어 키보드가 화면에 보이는지 확인한다.  


Activity activity = new Activity("io.appium.android.apis", ".view.PopupMenu1");  
패키지 이름, 액티비티 이름  
driver.startActivity(activity);  
실행할 액티비티가 다른 패키지에 속해 있다면 그 패키지의 앱을 실행시키고 액티비티를 연다.  
현재 열려 있는 액티비티는 종료시킨다.  
String activityName = driver.currentActivity();  
액티비티 이름을 리턴한다.(.view.PopupMenu1)  



